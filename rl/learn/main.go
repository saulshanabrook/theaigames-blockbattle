package learn

import (
	"math/rand"
	"sync"

	"github.com/NOX73/go-neural/persist"
	"github.com/Sirupsen/logrus"
	"github.com/saulshanabrook/blockbattle/game"
	"github.com/saulshanabrook/blockbattle/player"
	"github.com/saulshanabrook/blockbattle/rl/bot"
	bbEngine "github.com/saulshanabrook/blockbattle/rl/engine"
)

// this is the back prop "speed"
// https://github.com/NOX73/go-neural/blob/f327eff30de74b5b2d18236415bd35a2ee5c4e59/learn/learn.go#L47
// 0.9 just seemed like an OK constant
const nnSpeed = 0.9

// Learner handles all the agent state that we learn
type Learner struct {
	b    *bot.Bot
	exps *experiences
	c    LearnerConfig
}

type LearnerConfig struct {
	DiscountFactor float64
}

// copied from deepmind paper
var DefaultLearnerConfig = LearnerConfig{
	0.9,
}

// NewLearner creates a blank agent state
func NewLearner(c LearnerConfig) *Learner {
	return &Learner{
		bot.New(),
		newExperiences(),
		c,
	}
}

// RunEpisodes runs n epsidoes starting epsilon at 1 and bringing it linearly
// to 0.1 over the first 1/2 of the trainin and keeping it at 0.1 for the rest
func (l *Learner) RunEpisodes(n int) error {
	go l.startTraining()
	for i := 0; i < n; i++ {
		pComplete := float64(i) / float64(n)
		var pRandAct float64
		if pComplete < 0.5 {
			pRandAct = 1 - 0.9*pComplete*2
		} else {
			pRandAct = 0.1
		}
		logrus.WithFields(logrus.Fields{
			"p": pRandAct,
			"i": i,
		}).Info("Starting Episode")
		if err := l.RunEpisode(pRandAct); err != nil {
			return err
		}
	}
	return nil

}

// RunEpisode starts up one game and does learning on both players
func (l *Learner) RunEpisode(pRandAct float64) error {
	players, err := bbEngine.NewPlayers()
	if err != nil {
		return err
	}

	var wg sync.WaitGroup
	for _, p := range players {
		wg.Add(1)
		go func(p player.Player) {
			defer wg.Done()
			l.play(p, pRandAct)
		}(p)
	}
	wg.Wait()
	return nil
}

// Persist saves this NN to a file
func (l *Learner) Persist(filename string) {
	persist.DumpToFile(filename, l.b.Engine.Dump())
}

// play will play a game, taking the best action it can most of the time
// and a random one `pRandAct`% of the time.
// It will save all experiences it comes accross as well.
func (l *Learner) play(p player.Player, pRandAct float64) {
	// tell the engine we dont want to send any more moves once we finish
	defer close(p.Moves)
	st := <-p.States
	for {
		var loc game.Location
		var mvs []game.Move
		if rand.Float64() < pRandAct {
			// get a random action pRandAct% of the time
			loc, mvs = randAction(st)
		} else {
			// otherwise use our neural network to find the best action
			loc, mvs, _ = l.b.BestAction(st)
		}
		p.Moves <- mvs
		// get our next state so we can store this experience
		nextSt := <-p.States
		// got null value which means the states channel has closed
		// and game ended because of timeout. If this is the case
		// dont record anything and move on
		if (nextSt == game.State{}) {
			return
		}
		// By recording the current state, the action we took (loc), and the next
		// state, we can later train on this by computing the reward given the
		// two states
		l.recordExperience(st, loc, nextSt)

		// if the game has ended, stop playing
		if nextSt.IsOver() {
			return
		}
		st = nextSt
	}
}

func (l *Learner) startTraining() {
	go func() {
		<-l.exps.readyChan
		for {
			l.train()
		}
	}()
}

func (l *Learner) train() {
	exp := l.exps.pick()
	var nextVal float64
	if exp.nextSt.IsOver() {
		nextVal = 0
	} else {
		_, _, nextVal = l.b.BestAction(exp.nextSt)
	}

	l.b.Learn(
		exp.combFeatures,
		[]float64{exp.reward + l.c.DiscountFactor*nextVal},
		nnSpeed,
	)
}

func (l *Learner) recordExperience(st game.State, loc game.Location, nextSt game.State) {
	l.exps.add(&experience{
		combFeatures: append(bot.StateFeatures(st), bot.ActionFeatures(loc)...),
		reward:       reward(nextSt, st),
		nextSt:       nextSt,
	})
}
